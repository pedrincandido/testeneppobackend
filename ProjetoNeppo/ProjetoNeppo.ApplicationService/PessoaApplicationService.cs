﻿using System.Collections.Generic;
using ProjetoRestaurante.Domain.Commands.PessoaCommands;
using ProjetoRestaurante.Domain.Entities;
using ProjetoRestaurante.Domain.Repositories;
using ProjetoRestaurante.Domain.Services;
using ProjetoRestaurante.Infra.Persistence;

namespace ProjetoRestaurante.ApplicationService
{
    public class PessoaApplicationService : ApplicationService, IPessoaApplicationService
    {
        private readonly IPessoaRepository _repository;

        public PessoaApplicationService(IPessoaRepository repository, IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this._repository = repository;
        }

        public Pessoa Create(CreateAndUpdatePessoaCommand command)
        {
            var Pessoa = new Pessoa(command.Nome, command.Cpf, command.DataNascimento, command.SexoId);
            _repository.Create(Pessoa);

            if (Commit())
                return Pessoa;

            return null;
        }

        public Pessoa Delete(int id)
        {
            var Pessoa = _repository.Get(id);
            _repository.Delete(Pessoa);

            if (Commit())
                return Pessoa;

            return null;
        }

        public List<Pessoa> Get()
        {
            return _repository.Get();
        }

        public Pessoa Get(int id)
        {
            return _repository.Get(id);
        }

        public List<Pessoa> Get(int skip, int take)
        {
            return _repository.Get(skip, take);
        }

        public Pessoa Update(CreateAndUpdatePessoaCommand command)
        {
            var Pessoa = _repository.Get(command.Id);
            Pessoa.Update(command.Id, command.Nome, command.Cpf, command.DataNascimento,  command.SexoId);
            _repository.Update(Pessoa);

            if (Commit())
                return Pessoa;

            return null;
        }
    }
}
