﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoRestaurante.Infra.Persistence;

namespace ProjetoRestaurante.ApplicationService
{
    public class ApplicationService 
    {
        private IUnitOfWork _unitOfWork;


        public ApplicationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public bool Commit()
        {
            //if (_notifications.HasNotifications())
            //    return false;
            _unitOfWork.Commit();
            return true;
        }
    }
}
