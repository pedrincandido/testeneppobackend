﻿using System.Collections.Generic;
using ProjetoRestaurante.Domain.Commands.EnderecoCommands;
using ProjetoRestaurante.Domain.Commands.PessoaCommands;
using ProjetoRestaurante.Domain.Entities;
using ProjetoRestaurante.Domain.Repositories;
using ProjetoRestaurante.Domain.Services;
using ProjetoRestaurante.Infra.Persistence;

namespace ProjetoRestaurante.ApplicationService
{
    public class EnderecoApplicationService : ApplicationService, IEnderecoApplicationService
    {
        private readonly IEnderecoRepository _repository;

        public EnderecoApplicationService(IEnderecoRepository repository, IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            this._repository = repository;
        }

        public Endereco Create(CreateAndUpdateEnderecoCommand command)
        {
            var Endereco = new Endereco(command.Rua, command.Cep, command.Bairro, command.Numero, command.PersonId);
            _repository.Create(Endereco);

            if (Commit())
                return Endereco;

            return null;
        }

        public Endereco Delete(int id)
        {
            var Endereco = _repository.Get(id);
            _repository.Delete(Endereco);

            if (Commit())
                return Endereco;

            return null;
        }

        public List<Endereco> Get()
        {
            return _repository.Get();
        }

        public Endereco Get(int id)
        {
            return _repository.Get(id);
        }

        public List<Endereco> Get(int skip, int take)
        {
            return _repository.Get(skip, take);
        }

        public Endereco Update(CreateAndUpdateEnderecoCommand command)
        {
            var Endereco = _repository.Get(command.Id);
            Endereco.Update(command.Id, command.Rua, command.Cep, command.Bairro, command.Numero, command.PersonId);
            _repository.Update(Endereco);

            if (Commit())
                return Endereco;

            return null;
        }
    }
}
