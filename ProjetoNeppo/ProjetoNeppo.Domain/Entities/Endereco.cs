﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Entities
{
    [DataContract]
    public class Endereco
    {
        public Endereco(string rua, string cep, string bairro, int numero, int personId)
        {
            Rua = rua;
            Cep = cep;
            Bairro = bairro;
            Numero = numero;
            PersonId = personId;
        }

        [DataMember(Name = "id")]
        public int Id { get; private set; }

        [DataMember(Name = "rua")]
        public string Rua { get; private set; }

        [DataMember(Name = "numero")]
        public int Numero { get; private set; }

        [DataMember(Name = "bairro")]
        public string Bairro { get; private set; }

        [DataMember(Name = "cep")]
        public string Cep { get; private set; }


        [DataMember(Name = "person_id")]
        public int PersonId { get; private set; }

        public void Update(int id, string rua, string cep, string bairro, int numero, int personId)
        {
            Id = id;
            Rua = rua;
            Cep = cep;
            Bairro = bairro;
            Numero = numero;
            PersonId = personId;
        }
    }
}
