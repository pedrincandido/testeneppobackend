﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Entities
{
    [DataContract]
    public class Pessoa
    {
     
        public Pessoa(string nome, string cpf, DateTime dataNascimento, int sexoId)
        {
            this.Nome           = nome;
            this.Cpf            = cpf;
            this.DataNascimento = dataNascimento;
            this.SexoId         = sexoId;
        }

        [DataMember(Name = "id")]
        public int Id { get; private set; }

        [DataMember(Name = "nome")]
        public string Nome { get; private set; }

        [DataMember(Name = "dataNascimento")]
        public DateTime DataNascimento { get; private set; }

        [DataMember(Name = "sexo_id")]
        public int SexoId { get; private set; }

        [DataMember(Name = "sexo")]

        public Sexo Sexo { get; set; }

  
        public string Cpf { get; private set; }


        public void Update(int id, string nome, string cpf, DateTime dataNascimento, int sexoId)
        {
            this.Id             = id;
            this.Nome           = nome;
            this.DataNascimento = dataNascimento;
            this.SexoId         = sexoId;
            
        }

    }
}
