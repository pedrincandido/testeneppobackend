﻿using System.Runtime.Serialization;

namespace ProjetoRestaurante.Domain.Entities
{
    [DataContract]
    public class Sexo
    {
        [DataMember(Name = "id")]
        public int Id { get; private set; }

        [DataMember(Name = "nome")]
        public string Nome { get; private set; }
    }
}