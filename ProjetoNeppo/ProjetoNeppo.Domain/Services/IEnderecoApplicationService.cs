﻿using ProjetoRestaurante.Domain.Commands.EnderecoCommands;
using ProjetoRestaurante.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Services
{
    public interface IEnderecoApplicationService
    {
        List<Endereco> Get();
        List<Endereco> Get(int skip, int take);
        Endereco Get(int id);
        Endereco Create(CreateAndUpdateEnderecoCommand command);
        Endereco Update(CreateAndUpdateEnderecoCommand command);
        Endereco Delete(int id);
    }
}
