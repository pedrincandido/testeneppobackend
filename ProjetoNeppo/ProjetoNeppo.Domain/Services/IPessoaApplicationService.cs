﻿
using ProjetoRestaurante.Domain.Commands.PessoaCommands;
using ProjetoRestaurante.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Services
{
    public interface IPessoaApplicationService
    {
        List<Pessoa> Get();
        List<Pessoa> Get(int skip, int take);
        Pessoa Get(int id);
        Pessoa Create(CreateAndUpdatePessoaCommand command);
        Pessoa Update(CreateAndUpdatePessoaCommand command);
        Pessoa Delete(int id);
    }
}
