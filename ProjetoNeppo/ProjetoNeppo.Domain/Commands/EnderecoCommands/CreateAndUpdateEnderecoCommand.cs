﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Commands.EnderecoCommands
{
    public class CreateAndUpdateEnderecoCommand
    {
        public CreateAndUpdateEnderecoCommand(string rua, string cep, string bairro, int numero, int personId)
        {
            this.Rua = rua;
            this.Cep = cep;
            this.Bairro = bairro;
            this.Numero = numero;
            this.PersonId = personId;
          
        }

        public CreateAndUpdateEnderecoCommand(int id, string rua, string cep, string bairro, int numero, int personId)
        {
            this.Id = id;
            this.Rua = rua;
            this.Cep = cep;
            this.Bairro = bairro;
            this.Numero = numero;
            this.PersonId = personId;
        }

        public int Id { get; set; }
        public string Rua { get; set; }

        public string Cep { get; set; }

        public string Bairro { get; set; }
        public int Numero { get; set; }

        public int PersonId { get; set; }
    }
}
