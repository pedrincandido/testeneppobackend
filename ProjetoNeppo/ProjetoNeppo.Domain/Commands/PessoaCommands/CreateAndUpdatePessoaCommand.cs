﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Commands.PessoaCommands
{
    public class CreateAndUpdatePessoaCommand
    {
        public CreateAndUpdatePessoaCommand(string nome, string cpf, DateTime dataNascimento, int sexoId)
        {
            this.Nome = nome;
            this.Cpf = cpf;
            this.DataNascimento = dataNascimento;
            this.SexoId = sexoId;
        }

        public CreateAndUpdatePessoaCommand(int id, string nome, string cpf, DateTime dataNascimento, int sexoId)
        {
            this.Id = id;
            this.Nome = nome;
            this.Cpf = cpf;
            this.DataNascimento = dataNascimento;
            this.SexoId = sexoId;

        }

        public int Id { get; set; }

        public string Nome { get; set; }

        public string Cpf { get; set; }

        public DateTime DataNascimento { get; set; }
        public int SexoId { get; set; }
  
    }
}
