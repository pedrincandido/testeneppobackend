﻿using ProjetoRestaurante.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Repositories
{
    public interface IPessoaRepository
    {
        List<Pessoa> Get();
        List<Pessoa> Get(int skip, int take);
        Pessoa Get(int? id);
        void Create(Pessoa prato);
        void Update(Pessoa prato);
        void Delete(Pessoa prato);
    }
}
