﻿using ProjetoRestaurante.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetoRestaurante.Domain.Repositories
{
    public interface IEnderecoRepository
    {
        List<Endereco> Get();
        List<Endereco> Get(int skip, int take);
        Endereco Get(int id);
        void Create(Endereco prato);
        void Update(Endereco prato);
        void Delete(Endereco prato);
    }
}
