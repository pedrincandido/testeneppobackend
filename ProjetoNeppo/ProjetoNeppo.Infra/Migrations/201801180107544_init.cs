namespace ProjetoRestaurante.Infra.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ENDERECO",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Rua = c.String(nullable: false, maxLength: 150),
                        Numero = c.Int(nullable: false),
                        Bairro = c.String(nullable: false, maxLength: 50),
                        Cep = c.String(nullable: false, maxLength: 20),
                        PersonId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PESSOA",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 60),
                        DataNascimento = c.DateTime(nullable: false),
                        SexoId = c.Int(nullable: false),
                        Cpf = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SEXO", t => t.SexoId, cascadeDelete: true)
                .Index(t => t.SexoId);
            
            CreateTable(
                "dbo.SEXO",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(nullable: false, maxLength: 30),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PESSOA", "SexoId", "dbo.SEXO");
            DropIndex("dbo.PESSOA", new[] { "SexoId" });
            DropTable("dbo.SEXO");
            DropTable("dbo.PESSOA");
            DropTable("dbo.ENDERECO");
        }
    }
}
