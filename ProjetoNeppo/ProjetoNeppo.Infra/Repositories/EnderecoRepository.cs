﻿using System;
using ProjetoRestaurante.Domain.Entities;
using ProjetoRestaurante.Domain.Repositories;
using ProjetoRestaurante.Infra.Persistence.Contexts;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;


namespace ProjetoEndereco.Infra.Repositories
{
    public class EnderecoRepository : IEnderecoRepository
    {
        private readonly NeppoDataContext _context;

        public EnderecoRepository(NeppoDataContext context)
        {
            _context = context;
        }

        public List<Endereco> Get()
        {
            return _context.Enderecos.ToList();
        }

        public List<Endereco> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Endereco Get(int id)
        {
            return _context.Enderecos.Find(id);
        }

        public Endereco GetByNome(string nome)
        {
            //return _context.Pratos.Include(x => x.Prato).FirstOrDefault(x => x.Nome == nome);
            return null;
        }

        public void Delete(Endereco Endereco)
        {
            _context.Enderecos.Remove(Endereco);
        }

        //public List<Prato> GetByIdEndereco(int id)
        //{
        //    return _context.Pratos.Where(x => x. == id).ToList();
        //}

        public void Create(Endereco Endereco)
        {
            _context.Enderecos.Add(Endereco);
        }

        public void Update(Endereco Endereco)
        {
            _context.Entry<Endereco>(Endereco).State = EntityState.Modified;
        }
    }
}
