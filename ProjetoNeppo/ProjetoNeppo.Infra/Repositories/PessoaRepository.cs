﻿
using ProjetoRestaurante.Domain.Entities;
using ProjetoRestaurante.Domain.Repositories;
using ProjetoRestaurante.Infra.Persistence.Contexts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ProjetoRestaurante.Infra.Repositories
{
    public class PessoaRepository : IPessoaRepository
    {
        private readonly NeppoDataContext _context;

        public PessoaRepository(NeppoDataContext context)
        {
            _context = context;
        }

        public List<Pessoa> Get()
        {
            return _context.Pessoas.ToList();
        }

        //public List<Pessoa> GetByIdRestaurante(int id)
        //{
        //    return _context.Pessoa.Where(x => x.RestauranteId == id).ToList();
        //}
        public List<Pessoa> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public Pessoa Get(int id)
        {
            return _context.Pessoas.Find(id);
        }

        public Pessoa GetByName(string name)
        {
            return _context
                .Pessoas
                .Where(x => x.Nome == name)
                .FirstOrDefault();
        }

        public void Create(Pessoa Pessoa)
        {
            _context.Pessoas.Add(Pessoa);
        }

        public void Update(Pessoa Pessoa)
        {
            _context.Entry<Pessoa>(Pessoa).State = EntityState.Modified;
        }

        public void Delete(Pessoa Pessoa)
        {
            _context.Pessoas.Remove(Pessoa);
        }

        public Pessoa Get(int? id)
        {
            throw new NotImplementedException();
        }
    }
}
