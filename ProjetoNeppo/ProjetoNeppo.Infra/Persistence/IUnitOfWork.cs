﻿using System;

namespace ProjetoRestaurante.Infra.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();

    }
}
