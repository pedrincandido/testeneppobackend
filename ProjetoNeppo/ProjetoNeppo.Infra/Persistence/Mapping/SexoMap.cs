﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoRestaurante.Domain.Entities;

namespace ProjetoRestaurante.Infra.Persistence.Mapping
{
    public class SexoMap : EntityTypeConfiguration<Sexo>
    {
        public SexoMap()
        {
            ToTable("SEXO");

            HasKey(x => x.Id);
            Property(x => x.Nome).IsRequired().IsRequired().HasMaxLength(30);
        }
    }
}
