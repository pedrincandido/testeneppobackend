﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoRestaurante.Domain.Entities;

namespace ProjetoRestaurante.Infra.Persistence.Mapping
{
    public class EnderecoMap : EntityTypeConfiguration<Endereco>
    {
        public EnderecoMap()
        {
            ToTable("ENDERECO");

            HasKey(x => x.Id);
            Property(x => x.Rua).IsRequired().IsRequired().HasMaxLength(150);
            Property(x => x.Bairro).IsRequired().IsRequired().HasMaxLength(50);
            Property(x => x.Cep).IsRequired().IsRequired().HasMaxLength(20);
            Property(x => x.Numero).IsRequired().IsRequired();


            Property(x => x.PersonId).IsRequired();

        }
    }
}
