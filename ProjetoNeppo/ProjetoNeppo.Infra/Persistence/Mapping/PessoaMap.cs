﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjetoRestaurante.Domain.Entities;

namespace ProjetoRestaurante.Infra.Persistence.Mapping
{
    public class PessoaMap : EntityTypeConfiguration<Pessoa>
    {
        public PessoaMap()
        {
            ToTable("PESSOA");

            HasKey(x => x.Id);
            Property(x => x.Nome).IsRequired().IsRequired().HasMaxLength(60);
            Property(x => x.DataNascimento).IsRequired();
            Property(x => x.SexoId).IsRequired();
       
            HasRequired(x => x.Sexo);
        }
    }
}
