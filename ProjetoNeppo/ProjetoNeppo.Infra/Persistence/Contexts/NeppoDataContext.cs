﻿using System.Data.Entity;
using ProjetoRestaurante.Domain.Entities;
using ProjetoRestaurante.Infra.Persistence.Mapping;

namespace ProjetoRestaurante.Infra.Persistence.Contexts
{
    public class NeppoDataContext : DbContext
    {
        public NeppoDataContext() : base("TesteDB")
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }


        public DbSet<Pessoa> Pessoas { get; set; }

        public DbSet<Sexo> Sexos { get; set; }

        public DbSet<Endereco> Enderecos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
  
            modelBuilder.Configurations.Add(new PessoaMap());
            modelBuilder.Configurations.Add(new EnderecoMap());
            modelBuilder.Configurations.Add(new SexoMap());
        }
    }
}
