﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ProjetoEndereco.Infra.Repositories;
using ProjetoRestaurante.ApplicationService;
using ProjetoRestaurante.Domain.Repositories;
using ProjetoRestaurante.Domain.Services;
using ProjetoRestaurante.Infra.Persistence;
using ProjetoRestaurante.Infra.Persistence.Contexts;
using ProjetoRestaurante.Infra.Repositories;
using ProjetoRestaurante.Shared;


namespace ProjetoRestaurante.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = configurationBuilder.Build();

        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddCors();

            services.AddScoped<NeppoDataContext, NeppoDataContext>();

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<IPessoaRepository, PessoaRepository>();
            services.AddTransient<IEnderecoRepository, EnderecoRepository>();

            services.AddTransient<IPessoaApplicationService, PessoaApplicationService>();
            services.AddTransient<IEnderecoApplicationService, EnderecoApplicationService>();
     
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
            });

            app.UseMvc();

            Runtime.ConnectionString = Configuration.GetConnectionString("CnnStr");
        }
    }
}
