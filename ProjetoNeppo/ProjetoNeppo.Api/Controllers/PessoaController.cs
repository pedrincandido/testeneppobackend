﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using ProjetoRestaurante.Domain.Commands.PessoaCommands;
using ProjetoRestaurante.Domain.Services;
using ProjetoRestaurante.Infra.Persistence;

namespace ProjetoRestaurante.Api.Controllers
{
    public class PessoaController : BaseController
    {
        
        private readonly IPessoaApplicationService _service;
   

        public PessoaController(IPessoaApplicationService service, IUnitOfWork uow) : base(uow)
        {
            _service = service;
        }


        [HttpGet]
        [Route("pessoa/{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_service.Get(id));
        }

        [HttpGet]
        [Route("pessoa")]
        public IActionResult Get()
        {
            return Ok(_service.Get());
        }

        [HttpPost]
        [Route("pessoa")]
        public IActionResult Post([FromBody]dynamic body)
        {
            var command = new CreateAndUpdatePessoaCommand(
                nome: (string)body.Nome,
                cpf: (string)body.Cpf,
                dataNascimento: body.DataNascimento,
                sexoId: (int)body.SexoId
            );
            _service.Create(command);
            return Ok();
        }

        [HttpPut]
        //[Authorize]
        [Route("pessoa")]
        public IActionResult Put([FromBody]dynamic body)
        {
            var command = new CreateAndUpdatePessoaCommand(
                id: (int)body.id,
                nome: (string)body.Nome,
                cpf: (string)body.Cpf,
                dataNascimento: body.DataNascimento,
                sexoId: (int)body.SexoId
            );
            _service.Update(command);
            return Ok(HttpStatusCode.OK);
        }

        [HttpDelete]
        ////[Authorize]
        [Route("pessoa/{id}")]
        public IActionResult Delete(int id)
        {
            if (id > 0)
                _service.Delete(id);

            else
                BadRequest();

            return Ok(HttpStatusCode.OK);
        }
    }
}
