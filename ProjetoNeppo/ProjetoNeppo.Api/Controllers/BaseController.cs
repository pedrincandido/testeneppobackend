﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidator;
using Microsoft.AspNetCore.Mvc;
using ProjetoRestaurante.Infra.Persistence;


namespace ProjetoRestaurante.Api.Controllers
{
    public class BaseController : Controller
    {
        private readonly IUnitOfWork _uow;

        public BaseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<IActionResult> Response(object result, IEnumerable<Notification> notifications)
        {
            if (!notifications.Any())
            {
                try
                {
                    _uow.Commit();
                    return Ok(new
                    {
                        sucess = true,
                        data = result
                    });
                }
                catch
                {
                    return BadRequest(new
                    {
                        sucess = false,
                        errors = new[] { "Ocorreu uma falha interna no servidor" }
                    });
                }
            }
            else
            {
                return BadRequest(new
                {
                    sucess = false,
                    errors = notifications
                });
            }
        }
    }
}
