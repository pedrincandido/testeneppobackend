﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using ProjetoRestaurante.Domain.Commands.EnderecoCommands;
using ProjetoRestaurante.Domain.Services;
using ProjetoRestaurante.Infra.Persistence;

namespace ProjetoRestaurante.Api.Controllers
{
    public class EnderecoController : BaseController
    {
        
        private readonly IEnderecoApplicationService _service;
   

        public EnderecoController(IEnderecoApplicationService service, IUnitOfWork uow) : base(uow)
        {
            _service = service;
        }

        [HttpGet]
        [Route("api/Enderecos/{id}")]
        public IActionResult Get(int id)
        {
            return Ok(_service.Get(id));
        }

        [HttpGet]
        [Route("api/Enderecos")]
        public IActionResult Get()
        {
            return Ok(_service.Get());
        }

        [HttpPost]
        [Route("api/Enderecos")]
        public IActionResult Post([FromBody]dynamic body)
        {
            var command = new CreateAndUpdateEnderecoCommand(

                rua: (string)body.Rua,
                cep: (string)body.Cep,
                bairro: (string)body.Bairro,
                numero: (int)body.Numero,
                personId: (int)body.PersonId

           );
            _service.Create(command);
            return Ok();
        }

        [HttpPut]
        //[Authorize]
        [Route("api/Enderecos")]
        public IActionResult Put([FromBody]dynamic body)
        {
            var command = new CreateAndUpdateEnderecoCommand(
                id: (int)body.id,
                rua: (string)body.Rua,
                cep: (string)body.Cep,
                bairro: (string)body.Bairro,
                numero: (int)body.Numero,
                personId: (int)body.PersonId
            );
            _service.Update(command);
            return Ok(HttpStatusCode.OK);
        }

        [HttpDelete]
        ////[Authorize]
        [Route("api/Enderecos/{id}")]
        public IActionResult Delete(int id)
        {
            if (id > 0)
                _service.Delete(id);

            else
                BadRequest();

            return Ok(HttpStatusCode.OK);
        }
    }
}
